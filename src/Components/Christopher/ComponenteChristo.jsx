import React from 'react'
import './EstilosChristopher.css'
import elchristo from '../../assets/img/elchristo.jpeg'
import ComponenteChristoherGustos from '../ComponenteChristopherGustos/ComponenteChristopherGustos'

function ComponenteChristo() {
  return (
    <>
      <div className="cont">
        <div className="information">
          <h1>Hola soy <span>Christopher</span></h1>
          <p>Tengo 22 años y soy estudiante del Instituto Tecnológico de México campus Cuautla de la carrera de Ingeniería en Sistemas Computacionales.
            Actualmente me gusataria aprender más sobre el desarrollo web, especificamente en la parte del Frontend ya que a este punto de mi vida y de la carrera me
            gustaria dedicarme a esto.
          </p>
        </div>
        <div className="card-christo">
          <img src={elchristo} alt="Imagen de la card" />
        </div>
        <div className="component">
          <ComponenteChristoherGustos/>
        </div>
      </div>
    </>
  )
}
export default ComponenteChristo
