import React from 'react'
import './ComponenteMonse.css'
import monserrat from '../../assets/img/monserrat.jpeg'
import ComponenteMonseGustos from '../ComponenteMonseGustos/ComponenteMonseGustos'

function ComponenteMonse() {
  return (
    <>
      <div className="wrapper">
  <main className="main">
    <div className="info">
      <h1 className='titulo'>Hola soy <span>Monserrat</span></h1>
       <p className='texto'>Estudiante de Ingeniería en Sistemas Computacionales en el Instituto Tecnológico de Cuautla. Tengo 21 años y me apasiona el desarrollo web, 
         por lo que me he enfocado en aprender todo lo necesario para ser un profesional 
         Full Stack. </p>
    </div>
    <img src={monserrat} alt=""/>
  </main>
  <div className="componente">
    <ComponenteMonseGustos/>
  </div>
</div>
    </>
  )
}export default ComponenteMonse
