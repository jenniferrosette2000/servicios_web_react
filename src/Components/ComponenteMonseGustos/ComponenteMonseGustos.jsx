import React from 'react'
import MonseAnimal from '../../assets/img/MonseAnimal.jpg'
import MonseComida from '../../assets/img/MonseComida.jpg'
import MonseMusica from '../../assets/img/MonseMusica.jpg'
import MonsePasatiempo from '../../assets/img/MonsePasatiempo.jpg'
import MonseRandom from '../../assets/img/MonseRandom.jpg'
import './ComponenteMonseGustos.css'

function ComponenteMonseGustos() {
  return (
    <>
    <h3 className="subtitulo">Acerca de mí</h3>
    <hr/>
    <div className="card-group">
    <div className="card">
      <img className="card-img-top" src={MonseComida} alt="Card image cap"/>
      <div className="card-body">
      <h5 className="card-title">Comida</h5>
      <div className="divider"></div>
      <p className="card-text">Mi comida favorita son las Enchiladas.</p>
    </div>
    </div>
    <div className="card">
      <img className="card-img-top" src={MonseMusica} alt="Card image cap"/>
      <div className="card-body">
      <h5 className="card-title">Música</h5>
      <div className="divider"></div>
      <p className="card-text">Me gusta diferentes tipos de musica, sin embargo mi gusto en especial es el indie Mexicano.</p>
    </div>
    </div>
    <div className="card">
      <img className="card-img-top" src={MonsePasatiempo}alt="Card image cap"/>
      <div className="card-body">
      <h5 className="card-title">Pasatiempo</h5>
      <div className="divider"></div>
      <p className="card-text">No suelo tener pasatiempos, pero algo que me gusta hacer es conocer nuevo lugares.</p>
    </div>
    </div>
    <div className="card">
      <img className="card-img-top" src={MonseAnimal} alt="Card image cap"/>
      <div className="card-body">
      <h5 className="card-title">Animal</h5>
      <div className="divider"></div>
      <p className="card-text">Tengo 2 animales favoritos, El perro y la mariposa.</p>
    </div>
    </div>
    <div className="card">
      <img className="card-img-top" src={MonseRandom} alt="Card image cap"/>
      <div className="card-body">
      <h5 className="card-title">Random</h5>
      <div className="divider"></div>
      <p className="card-text">Dentro de mi carrera, me agrada mucho la parte del Frontend.</p>
    </div>
    </div>
    </div>
    </>
    )
}

export default ComponenteMonseGustos