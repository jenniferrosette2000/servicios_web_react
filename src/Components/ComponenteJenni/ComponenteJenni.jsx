import React from 'react'
import './ComponenteJenni.css'
import jenni1 from '../../assets/img/jenni1.png'
import jenni2 from '../../assets/img/jenni2.png'
import jenni3 from '../../assets/img/jenni3.png'
import jenni4 from '../../assets/img/jenni4.png'
import jenni5 from '../../assets/img/jenni5.png'
function ComponenteJenni() {
  return (
    <div className='cards'>
        <div className="card-ja">
            <div className="head-ja">
                <div className="circle-ja"></div>
                <div className="img-ja">
                    <img src={jenni1} alt=""/>
                </div>
            </div>
    
            <div className="description-ja">
                <h4>COMIDA</h4>
                <p>Mi comida favorita son las alitas en BBQ con arroz blanco</p>
            </div>
        </div>
    
        <div className="card-ja">
            <div className="head-ja">
                <div className="circle-ja"></div>
                <div className="img-ja">
                    <img src={jenni2} alt=""/>
                </div>
            </div>
    
            <div className="description-ja">
                <h4>MUSICA</h4>
                <p>Me gusta casi todo tipo de musica y la escucho dependiendo la situacion</p>
            </div>
        </div>
    
        <div className="card-ja">
            <div className="head-ja">
                <div className="circle-ja"></div>
                <div className="img-ja">
                    <img src={jenni3} alt=""/>
                </div>
            </div>
    
            <div className="description-ja">
                <h4>PASATIEMPO</h4>
                <p>En mi tiempo libre me gusta crear y diseñar accesorios de bisuteria</p>
            </div>
        </div>
    
        <div className="card-ja">
            <div className="head-ja">
                <div className="circle-ja"></div>
                <div className="img-ja">
                    <img src={jenni4} alt=""/>
                </div>
            </div>
    
            <div className="description-ja">
                <h4>ANIMAL</h4>
                <p>Me encantan los perros siento que son animales muy leales y de hecho tengo 7 en casa</p>
            </div>
        </div>
        
    
        <div className="card-ja">
            <div className="head-ja">
                <div className="circle-ja"></div>
                <div className="img-ja">
                    <img src={jenni5} alt=""/>
                </div>
            </div>
    
            <div className="description-ja">
                <h4>QUISIERA</h4>
                <p>Me gusta mucho la navidad que quisiera que todos los dias lo fueran</p>
            </div>
        </div>
        </div>
    
  )
} export default ComponenteJenni
