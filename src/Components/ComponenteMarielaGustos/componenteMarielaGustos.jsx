import React from 'react'
import './componenteMarielaGustos.css'

function Mar() {
    return (
        <>
            <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
                <div className="col">
                    <div className="card-M">
                        <img src="src/assets/img/animalm.jpg" class="card-img-top" alt="..." />
                        <div className="description">
                            <h4>ANIMAL</h4>
                            <p className="card-text">Mi animal favorito es el gato. Actualmente tengo 5 en casa. 
                            </p>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card-M">
                        <img src="src/assets/img/comidam.jpg" class="card-img-top" alt="..." />
                        <div className="description">
                            <h3>COMIDA</h3>
                            <p className="card-text">Mi comida favorita son los tacos y alambres de pastor</p>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card-M">
                        <img src="src/assets/img/seriem.jpg" class="card-img-top" alt="..." />
                        <div className="description">
                            <h4>SERIE</h4>
                            <p className="card-text">Mi serie favorita es Stranger Things pero también me gustan las películas animadas.</p>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card-M">
                        <img src="src/assets/img/musicam.jpg" class="card-img-top" alt="..." />
                        <div className="description">
                            <h4>MUSICA</h4>
                            <p className="card-text">Me gusta en su mayoría todo tipo de musica pero por el momento me gusta más el rock mexicano, música folk y alternativa</p>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card-M">
                        <img src="src/assets/img/pasatiempom.jpg" class="card-img-top" alt="..." />
                        <div className="description">
                            <h4>PASATIEMPO</h4>
                            <p className="card-text">Me gusta mucho pasar mi tiempo libre prácticando jardinería.</p>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card-M">
                        <img src="src/assets/img/datom.jpg" className="card-img-top" alt="..." />
                        <div className="description">
                            <h4>DATO CURIOSO</h4>
                            <p className="card-text">Me gustan las perforaciones y mi flor favorita es el girasol</p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Mar
