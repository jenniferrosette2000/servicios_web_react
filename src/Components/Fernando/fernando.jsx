import React from 'react'
import './fernando.css'
import Fernando from '../componenteFernando/componenteFer'

function Fer() {
    return (
        <>
            <div className='fer'>
                <div className="information-Fer">
                    <h1>Hola soy Fernando</h1>
                    <p>Tengo 21 años y soy estudiante del Instituto Tecnológico de México Campus Cuautla de la carrera de Ingeniería en Sistemas Computacionales.
                        Me gusta echar cotorreo con la bandita, los carros y el fútbol.
                    </p>
                </div>
                <div className="card-Fer">
                    <img src="src/assets/img/fernando.jpg" alt="" />
                </div>

                <div class="component-Fer">
                    <Fernando />
                </div>
            </div>
        </>
    )
}

export default Fer
