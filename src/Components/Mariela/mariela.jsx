import React from 'react'
import './mariela.css'
import Mariela from '../componenteMarielaGustos/componenteMarielaGustos'

function Mari() {
    return (
        <>

            <div className='mariela'>
                <div className="information-mar">
                    <h1>Hola, me llamo Mariela</h1>
                    <p>Tengo 22 años, estudio en el Instituto Tecnológico de Cuautla y estoy en 8vo semestre de la carrera de Ingeniería en Sistemas Computacionales
                    </p>
                </div>
                <div className="card-mar">
                    <img src="src/assets/img/marielaa.jpg" alt="" />
                </div>

                <div class="component-Mar">
                    <Mariela />
                </div>
            </div>
        </>
    )
}

export default Mari
