import React from 'react'
import jesus1 from '../../assets/img/jesus-img1.png'
import jesus2 from '../../assets/img/jesus-img2.jpg'
import jesus3 from '../../assets/img/jesus-img3.jpg'
import jesus4 from '../../assets/img/jesus-img4.jpeg'
import jesus5 from '../../assets/img/jesus-img5.jpg'
import jesus6 from '../../assets/img/jesus-img6.png'
import './ComponenteJesusGustos.css'

function ComponenteJesusGustos() {
  return (
    <>
    <div className='jtitle'>Acerca de mí<h3></h3></div>


<div className="jcard-group">
  <div className="jcards">
    <div className="jcard-body">
    <img src={jesus1} alt=""/>
      <h5 className="jcard-title">Comida</h5>
      <div className="jlinea"></div>
      <p className="jcard-text">
        Me gustan mucho los tacos, pero mi favorita son las enchiladas.
      </p>
    </div>
  </div>

  <div className="jcards">
    <div className="jcard-body">
    <img src={jesus2} alt=""/>
      <h5 className="jcard-title">Música</h5>
      <div className="jlinea"></div>
      <p className="jcard-text">
        Me gusta casi todo tipo de música, pero lo que mas suelo escuchar es hip-hop y rock.
      </p>
    </div>
  </div>

  <div className="jcards">
    <div className="jcard-body">
    <img src={jesus3} alt=""/>
      <h5 className="jcard-title">Pasatiempo</h5>
      <div className="jlinea"></div>
      <p className="jcard-text">
        Lo que mas me gusta hacer en mi tiempo libre es jugar CSGO.
      </p>
    </div>
  </div>
  </div>
  <div className="jcard-group">
    <div className="jcards">
      <div className="jcard-body">
      <img src={jesus4} alt=""/>
        <h5 className="jcard-title">Animal</h5>
        <div className="jlinea"></div>
        <p className="jcard-text">
          Mi animal favorito es el pingüino.
        </p>
      </div>
    </div>

    <div className="jcards">
      <div className="jcard-body">
      <img src={jesus5} alt=""/>
        <h5 className="jcard-title">Deporte</h5>
        <div className="jlinea"></div>
        <p className="jcard-text">
          Ya no realizo ningún deporte, pero me gusta el Basquetbol.
        </p>
      </div>
    </div>

    <div className="jcards">
      <div className="jcard-body">
      <img src={jesus6} alt=""/>
        <h5 className="jcard-title">Serie</h5>
        <div className="jlinea"></div>
        <p className="jcard-text">
          Mi serie favorita es la de Breaking bad
        </p>
      </div>
    </div>
  </div>


    </>
    )
}

export default ComponenteJesusGustos