import React from 'react'
import './ComponenteJesus.css'
import jesus from '../../assets/img/jesus.jpg'
import ComponenteJesusGustos from '../ComponenteJesusGustos/ComponenteJesusGustos'

function ComponenteMonse() {
  return (
    <>

<div className="section">
    <div className="description">
        <h1>Hola soy <span className='name'>Jesus</span></h1>
        <br/>
        <p>Tengo 22 años y soy estudiante del Instituto Tecnológico de México campus Cuautla de la carrera de Ingeniería en Sistemas Computacionales.
             Actualmente me gustaría  aprender más sobre el desarrollo web y especializarme en la rama de Fronted.
        </p>
    </div>
    <div className="card-image">
    <img src={jesus} alt=""/>
    </div>
    <div className="jcomponent">
    <ComponenteJesusGustos/>
    </div>
</div>

    </>
  )
}export default ComponenteMonse
