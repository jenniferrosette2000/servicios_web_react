import React from 'react'
import './EstilosChristopherGustos.css'

import elchristotacos from '../../assets/img/elchristotacos.jpg'
import elchristoJuniorH from '../../assets/img/elchristoJuniorH.jpeg'
import elchristoPerritos from '../../assets/img/elchristoPerritos.jpg'
import elchristofortnite from '../../assets/img/elchristofortnite.jpg'
import elchristoVolley from '../../assets/img/elchristoVolley.jpg'
import elchristoS from '../../assets/img/elchristoS.jpg'


function ComponenteChristopherGustos() {
  return (
    <>
      <h3 className="sub">Más sobre el <span>Christo</span></h3>
      <hr />
      <br />
      <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
        <div className="col">
          <div className="card-ch">
            <img src={elchristotacos} className="card-img-top-ch" alt="..." />
            <div className="card-body-ch">
              <h5 className="card-title-ch">Comida</h5>
              <p className="card-text-ch">Normalmente me gustan mucho los tacos al pastor. Es lo mejor
                que puede tener México.
              </p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card-ch">
            <img src={elchristoJuniorH} className="card-img-top-ch" alt="..." />
            <div className="card-body-ch">
              <h5 className="card-title-ch">Musica</h5>
              <p className="card-text-ch">Actualmente me gusta escuchar los corridos tumbados, me pone activo y me hace sentir que
                puedo con todo.</p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card-ch">
            <img src={elchristoPerritos} className="card-img-top-ch" alt="..."/>
              <div className="card-body-ch">
                <h5 className="card-title-ch">Mascota</h5>
                <p className="card-text-ch">Me gustan los perros porque son los unicos que pueden ofrecernos lealtad, tengo 6 perritos.
                </p>
              </div>
          </div>
        </div>
        <div className="col">
          <div className="card-ch">
            <img src={elchristofortnite} className="card-img-top-ch" alt="..." />
            <div className="card-body-ch">
              <h5 className="card-title-ch">Pasatiempo</h5>
              <p className="card-text-ch">Me gusta jugar con mis compadres al Fortnite porque me divierto con ellos.</p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card-ch">
            <img src={elchristoVolley} className="card-img-top-ch" alt="..." />
            <div className="card-body-ch">
              <h5 className="card-title-ch">Deporte</h5>
              <p className="card-text-ch">Me gusta jugar el Volleyball, en la prepa ganamos la final
                y fue la mejor final que haya podido jugar.
              </p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card-ch">
            <img src={elchristoS} className="card-img-top-ch" alt="..." />
            <div className="card-body-ch">
              <h5 className="card-title-ch">Mi enfoque</h5>
              <p className="card-text-ch">Trato de hacer cosas productivas y enfocarme en mi para evitar el sobrepensamiento.</p>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export default ComponenteChristopherGustos