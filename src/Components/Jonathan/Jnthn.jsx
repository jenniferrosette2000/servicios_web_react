import React from 'react'
import './jnthnEstilos.css'

function Jnthn() {
  return (
    <>
    
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet"/>

    <section>
       <div class="container1">
            <div class="name">
                <img src="src\assets\imgs-jnthn\user.jpeg" alt="" srcset=""/>
                <div class="info">
                    <p className = "enc"> Jonathan Espinosa Garcia </p>
                    <p className = "enc">22 años</p>
                    <p className = "enc">Estudiante </p>                    
                </div>
            </div>

        </div>

        <div class="container2">
            <div class="cardd">
                <p className = "pa">Mis comida favorita:</p>
                <span className='sp'>Tacos de suadero</span>
                <img src="src/assets/imgs-jnthn/tacos.jpg" alt=""/>
            </div>

            <div class="cardd">
                <p className = "pa">Dato curioso:</p>
                <span className='sp'>Solo fui 1 dia a la prepa</span>
                <img src="src/assets/imgs-jnthn/prepa.jpg" alt=""/>
            </div>

            <div class="cardd">
                <p className = "pa">Cancion favorita:</p>
                <span className='sp'>Ojos color sol</span>
                <img src="src/assets/imgs-jnthn/cancion.jpg" alt=""/>
            </div>

            <div class="cardd">
                <p className = "pa">Queria estudiar:</p>
                <span className='sp'>Arquitectura</span>
                <img src="src/assets/imgs-jnthn/arquitectura.jpg" alt=""/>
            </div>

        </div>

        <div class="container3">
            <div class="cardd-large">
                <p className = "pa">Pasa tiempos:</p>
                <div class="areas">

                    <div class="cardds">
                        <span className='sp'>Juegos</span>
                        <img src="src/assets/imgs-jnthn/sons.jpg" alt=""/>
                    </div>
    
                    <div class="cardds">
                        <span className='sp'>Podcast</span>
                        <img src="src/assets/imgs-jnthn/podcast.png" alt=""/>
                    </div>

                    <div class="cardds">
                        <span className='sp'>Anime</span>
                        <img src="src/assets/imgs-jnthn/tanjiro.jpg" alt=""/>
                    </div>
                </div>
            </div>


            <div class="cardd-large">
                <p className = "pa">Mis hijos:</p>
                <div class="areas">

                    <div class="cardds">
                        <span className='sp'>Pelusa</span>
                        <img src="src/assets/imgs-jnthn/pelusa.JPEG" alt=""/>
                    </div>
    
                    <div class="cardds">
                        <span className='sp'>Botitas</span>
                        <img src="src/assets/imgs-jnthn/botitas.jpeg" alt=""/>
                    </div>

                    <div class="cardds">
                        <span className='sp'>Pancho</span>
                        <img src="src/assets/imgs-jnthn/pancho.JPEG" alt=""/>
                    </div>
                </div>
            </div>

        </div>
    </section>

    </>
  )
}

export default Jnthn