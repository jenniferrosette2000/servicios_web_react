import './App.css'
import ComponenteChristo from './Components/Christopher/ComponenteChristo'
import ComponenteJenni from './Components/ComponenteJenni/ComponenteJenni'
import ComponenteJenniGustos from './Components/ComponenteJenniGustos/ComponenteJenniGustos'
import Mariela from './Components/Mariela/Mariela'
import ComponenteMonse from './Components/Monse/ComponenteMonse'
import ComponenteJesus from './Components/Jesus/ComponenteJesus'
import Jnthn from './Components/Jonathan/Jnthn'
import Fer from './Components/Fernando/fernando'



function App() {
  return (
    <>
      <div>
        <ComponenteJenniGustos/>
      </div>
      <div >
        <ComponenteJenni/>
      </div>

      <div>
        <ComponenteMonse/>
      </div>

      <div>
        <ComponenteChristo/>
      </div>

      <div>
        <ComponenteJesus/>
      </div>

      <div>
      <Jnthn/>
      </div>

      <div>
      <Fer/>
      </div>
      <div>
        <Mariela/>
      </div>
    </>

  )
}

export default App
